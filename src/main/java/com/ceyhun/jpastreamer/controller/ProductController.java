package com.ceyhun.jpastreamer.controller;

import com.ceyhun.jpastreamer.model.Product;
import com.ceyhun.jpastreamer.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {
    private final ProductService productService;

    @GetMapping("/{id}")
    public Product getById(@PathVariable Long id){
        return productService.findById(id);
    }

    @GetMapping("/getAll")
    public List<Product> getAll(){
        return productService.findAll();
    }

    @GetMapping("/productPrice/{cost}")
    public List<Product> priceGreaterThan(@PathVariable Double cost){
        return productService.findProductWhichExpensiveThan(cost);
    }
}
