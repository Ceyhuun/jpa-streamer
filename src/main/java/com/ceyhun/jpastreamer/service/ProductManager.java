package com.ceyhun.jpastreamer.service;

import com.ceyhun.jpastreamer.model.Product;
import com.ceyhun.jpastreamer.model.Product$;
import com.speedment.jpastreamer.application.JPAStreamer;
import com.speedment.jpastreamer.streamconfiguration.StreamConfiguration;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductManager implements ProductService{

    private final JPAStreamer jpaStreamer;
    @Override
    public List<Product> findAll() {
        return jpaStreamer.stream(Product.class)
                .sorted(Product$.id.reversed())
                .collect(Collectors.toList());
    }

    @Override
    public Product findById(Long id) {
        return jpaStreamer.stream(Product.class)
                .filter(Product$.id.equal(id))
                .findFirst()
                .orElseThrow();
    }

    @Override
    public List<Product> findProductWhichExpensiveThan(Double cost) {
        return jpaStreamer.stream(Product.class)
                .filter(Product$.price.greaterThan(cost))
                .sorted(Product$.price)
                .collect(Collectors.toList());
    }


}
