package com.ceyhun.jpastreamer.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductCategoryId implements Serializable {
    private static final long serialVersionUID = -3004116513732195464L;
    private Long productId;
    private Long categoryId;
}
