package com.ceyhun.jpastreamer.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@Table(name = "product_category")
public class ProductCategory {

    @EmbeddedId
    private ProductCategoryId productCategoryId;

    @ManyToOne
    @MapsId("productId")
    @JsonManagedReference
    private Product product;

    @ManyToOne
    @MapsId("categoryId")
    private Category category;

}
