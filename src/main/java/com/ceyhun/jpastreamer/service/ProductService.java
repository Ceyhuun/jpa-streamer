package com.ceyhun.jpastreamer.service;


import com.ceyhun.jpastreamer.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<Product> findAll();
    Product findById(Long id);
    List<Product> findProductWhichExpensiveThan(Double cost);
}
