INSERT INTO PRODUCT(ID, NAME, PRICE) VALUES(1, 'samsung', 70.0);
INSERT INTO PRODUCT(ID, NAME, PRICE) VALUES(2, 'iphone', 150.0);
INSERT INTO PRODUCT(ID, NAME, PRICE) VALUES(3, 'bmw', 250.0);
INSERT INTO PRODUCT(ID, NAME, PRICE) VALUES(4, 'mercedes', 90.0);
INSERT INTO PRODUCT(ID, NAME, PRICE) VALUES(5, 'asus', 200.0);
INSERT INTO PRODUCT(ID, NAME, PRICE) VALUES(6, 'hp', 80.0);

INSERT INTO CATEGORY(ID, NAME) VALUES(1, 'phone');
INSERT INTO CATEGORY(ID, NAME) VALUES(2, 'car');
INSERT INTO CATEGORY(ID, NAME) VALUES(3, 'laptop');

INSERT INTO PRODUCT_CATEGORY(PRODUCT_ID, CATEGORY_ID) VALUES(1, 1);
INSERT INTO PRODUCT_CATEGORY(PRODUCT_ID, CATEGORY_ID) VALUES(2, 1);
INSERT INTO PRODUCT_CATEGORY(PRODUCT_ID, CATEGORY_ID) VALUES(3, 2);
INSERT INTO PRODUCT_CATEGORY(PRODUCT_ID, CATEGORY_ID) VALUES(4, 2);
INSERT INTO PRODUCT_CATEGORY(PRODUCT_ID, CATEGORY_ID) VALUES(5, 3);
INSERT INTO PRODUCT_CATEGORY(PRODUCT_ID, CATEGORY_ID) VALUES(6, 3);
